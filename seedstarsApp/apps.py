from django.apps import AppConfig


class SeedstarsappConfig(AppConfig):
    name = 'seedstarsApp'
