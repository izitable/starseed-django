from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from seedstarsApp import forms
from seedstarsApp import models


# Create your views here.

def index(request):
    return render(request, 'index.html')


def add(request):
    if request.method == 'GET':
        form = forms.SeedstarsUserForm()
        return render(request, 'add.html', {'form': form})
    elif request.method == 'POST':
        form = forms.SeedstarsUserForm(request.POST)
        if form.is_valid():
            models.SeedstarsUser.objects.create(email=form.cleaned_data['email'], name=form.cleaned_data['name'])
            return HttpResponseRedirect('list')


def list(request):
    users = models.SeedstarsUser.objects.all()
    return render(request, 'list.html', {'users': users})
