from django import forms


class SeedstarsUserForm(forms.Form):
    name = forms.CharField(label='Name', max_length=20, required=True)
    email = forms.EmailField(label='Email', max_length=100, required=True)
